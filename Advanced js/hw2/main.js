const books = [
  {
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70
  },
  {
      author: "Скотт Бэккер",
      name: "Воин-пророк",
  },
  {
      name: "Тысячекратная мысль",
      price: 70
  },
  {
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
  },
  {
      author: "Дарья Донцова",
      name: "Детектив на диете",
      price: 40
  },
  {
      author: "Дарья Донцова",
      name: "Дед Снегур и Морозочка",
  }
];

function createList(book) {
  const root = document.createElement('div')
  const bookListItem = document.createElement("li");
  const propertyList = document.createElement("ul");
  const bookList = document.createElement("ul");
  document.body.append(root);
  root.setAttribute('id', 'root');
  root.append(bookList);

  for (const property in book) {
      const propertyItem = document.createElement("li").textContent = `${book[property]}  . ${""}`;
      propertyList.append(propertyItem);
  }
  bookListItem.append(propertyList);
  bookList.append(bookListItem);
}

function errorSearch(item) {
  item.map(book => {
      try {
          ["author", "name", "price"].map(property => {
              if (!book.hasOwnProperty(property)) {
                  throw new Error(`${property} is not found`);
              }
          });
          createList(book);
      } catch (e) {
          console.log(e);
      }
  });
}
errorSearch(books);
